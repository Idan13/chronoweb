package fr.idan.chrono.backend.repo;

import fr.idan.chrono.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    void deleteUserById(Long id);

    Optional<User> findUserById(Long id);

    @Query("select u from User u where u.email = ?1 and u.password = ?2")
    Optional<User> findUserByEmailAndPassword(String email, String password);

}
