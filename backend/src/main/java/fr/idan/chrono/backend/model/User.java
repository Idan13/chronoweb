package fr.idan.chrono.backend.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class User implements Serializable {

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, updatable = true, unique = true, length = 50)
    private String email;

    @Column(nullable = false, updatable = false)
    private String password;

    // date de creation du user
    // id unique

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
