package fr.idan.chrono.backend.exception;

public class LicenseNotFoundException extends RuntimeException{
    public LicenseNotFoundException(String message) {
        super(message);
    }
}
