package fr.idan.chrono.backend.service;

import fr.idan.chrono.backend.exception.UserAlreadyExistsException;
import fr.idan.chrono.backend.exception.UserNotFoundException;
import fr.idan.chrono.backend.model.User;
import fr.idan.chrono.backend.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserService {
    private final UserRepo UserRepo;

    @Autowired
    public UserService(UserRepo UserRepo) {
        this.UserRepo = UserRepo;
    }

    public User authenticateUser(User user) {
        return UserRepo.findUserByEmailAndPassword(user.getEmail(), user.getPassword()).orElseThrow(() -> new UserNotFoundException("Utilisateur " + user.getEmail() + " inconnu"));
    }

    public User addUserIfNotExists(User user) {
        try {
            return UserRepo.save(user);
        }
        catch (DataIntegrityViolationException violationException){
            throw new UserAlreadyExistsException("Un utilisateur ayant pour adresse email " + user.getEmail() + " existe déjà");
        }
    }

    public List<User> findAllUsers() {
        return UserRepo.findAll();
    }

    public User updateUser(User user) {
        return UserRepo.save(user);
    }

    public User findUserById(Long id) {
        return UserRepo.findUserById(id).orElseThrow(() -> new UserNotFoundException("User by id " + id + " not found"));
    }

    public void deleteUser(Long id) {
        UserRepo.deleteUserById(id);
    }


}
