package fr.idan.chrono.backend.endpoint;

import fr.idan.chrono.backend.exception.UserAlreadyExistsException;
import fr.idan.chrono.backend.exception.UserNotFoundException;
import fr.idan.chrono.backend.model.User;
import fr.idan.chrono.backend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserResource {

    private final UserService UserService;

    public UserResource(UserService UserService) {
        this.UserService = UserService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity authenticateUser(@RequestBody User user) {
        try {
            User authenticatedUser = UserService.authenticateUser(user);
            return ResponseEntity.ok(authenticatedUser);
        }catch (UserNotFoundException userNotFoundException){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(userNotFoundException);
        }
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        try {
            User createdUser = UserService.addUserIfNotExists(user);
            return ResponseEntity.ok(createdUser);
        }catch (UserAlreadyExistsException userAlreadyExistsException){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(userAlreadyExistsException);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAllUseres() {
        List<User> Useres = UserService.findAllUsers();
        return new ResponseEntity<>(Useres, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
        User User = UserService.findUserById(id);
        return new ResponseEntity<>(User, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody User User) {
        User createdUser = UserService.addUserIfNotExists(User);
        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<User> udapteUser(@RequestBody User User) {
        User updatedUser = UserService.updateUser(User);
        return new ResponseEntity<>(updatedUser, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        UserService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
