package fr.idan.chrono.backend.endpoint;

import fr.idan.chrono.backend.helper.CryptoUtils;
import fr.idan.chrono.backend.model.License;
import fr.idan.chrono.backend.model.licence.LicenceForm;
import fr.idan.chrono.backend.model.licence.LicenceResponse;
import fr.idan.chrono.backend.service.LicenseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/license")
public class LicenseResource {

    public static final String NO_LICENCE = "NO LICENCE";
    public static final String MY_LICENCE_IS_VALID = "LICENCE IS VALID";
    public static final String MY_LICENCE_IS_NOT_VALID = "LICENCE IS NOT VALID";
    private final LicenseService licenseService;

    public LicenseResource(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<License>> getAllLicenses() {
        List<License> licenses = licenseService.findAllLicenses();
        return new ResponseEntity<>(licenses, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<License> getLicenseById(@PathVariable("id") Long id) {
        License license = licenseService.findLicenseById(id);
        return new ResponseEntity<>(license, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<License> addLicense(@RequestBody License license) {

//        // create license
//        License license = new License();
//
//        // email
//        license.setEmail(email);
//
//        // type
//        license.setType(type);

        // date
        license.setDate(new Date());

        // init not in use
        license.setInUse(false);

        // create private key
        PrivateKey privateKey = null;
        try {
            privateKey = CryptoUtils.makeKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        assert privateKey != null;

        String privateKeyAsString = CryptoUtils.getKeyAsString(privateKey);

        license.setPrivateKey(privateKeyAsString);

        // create hash
        String hash = CryptoUtils.createLicence();
        license.setHash(hash);

        // Send Mail with encrypted hash
        CryptoUtils.encryptHash(hash, privateKey);

        License createdLicense = licenseService.addLicense(license);
        return new ResponseEntity<>(createdLicense, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<License> udapteLicense(@RequestBody License license) {
        License updatedLicense = licenseService.updateLicense(license);
        return new ResponseEntity<>(updatedLicense, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteLicense(@PathVariable("id") Long id) {
        licenseService.deleteLicense(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/check")
    public ResponseEntity<LicenceResponse> checkLicence(@RequestBody LicenceForm licenceForm) {

        LicenceResponse response = new LicenceResponse();

        if (licenceForm.getHash().isEmpty()) {
            response.setMessage(CryptoUtils.encrypt(NO_LICENCE));
        } else {

            String hash;
            String pcName;
            String clientId;
            try {

                hash = CryptoUtils.decrypt(licenceForm.getHash());
                pcName = CryptoUtils.decrypt(licenceForm.getPcName());
                clientId = CryptoUtils.decrypt(licenceForm.getClientId());

                // find licence in database
                this.licenseService.findLicenseByHash(hash);

                // if licence was found

                // if no pcname && no client id
                // save it

                // if licence has pc name
                // check pc name = pcName

                // if licence has client id
                // check client id is equal

                // licence was found and client id / pc name are matching or was first check
                response.setMessage(CryptoUtils.encrypt(MY_LICENCE_IS_VALID));

                // TODO

                // if licence was not found
                // response.setMessage(CryptoHelper.encrypt(MY_LICENCE_IS_NOT_VALID));


            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            }
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
