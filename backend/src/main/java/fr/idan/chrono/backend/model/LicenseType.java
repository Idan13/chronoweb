package fr.idan.chrono.backend.model;

public enum LicenseType {
    PRO, BASIC, BUSINESS, FREE;
}
