package fr.idan.chrono.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class License {

    public License() {
    }

    public License(String email, String type, Date date, String pc, Boolean inUse) {
        this.email = email;
        this.type = type;
        this.date = date;
        this.pc = pc;
        this.inUse = inUse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm")
    private Date date;

    @Column
    private String pc;

    @Column
    private Boolean inUse;

    @Column
    private String hash;

    @Column
    private String privateKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public String toString() {
        return "License{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", type='" + type + '\'' +
                ", date=" + date +
                ", pc='" + pc + '\'' +
                ", inUse=" + inUse +
                ", hash='" + hash + '\'' +
                ", privateKey='" + privateKey + '\'' +
                '}';
    }
}
