package fr.idan.chrono.backend.model.licence;

public class LicenceResponse {
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
