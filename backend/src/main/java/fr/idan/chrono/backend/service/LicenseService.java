package fr.idan.chrono.backend.service;

import fr.idan.chrono.backend.exception.LicenseNotFoundException;
import fr.idan.chrono.backend.model.License;
import fr.idan.chrono.backend.repo.LicenseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LicenseService {
    private final LicenseRepo licenseRepo;

    @Autowired
    public LicenseService(LicenseRepo licenseRepo) {
        this.licenseRepo = licenseRepo;
    }

    public License addLicense(License license) {
        return licenseRepo.save(license);
    }

    public List<License> findAllLicenses() {
        return licenseRepo.findAll();
    }

    public License updateLicense(License license) {
        return licenseRepo.save(license);
    }

    public License findLicenseById(Long id) {
        return licenseRepo.findLicenseById(id).orElseThrow(() -> new LicenseNotFoundException("License by id " + id + " not found"));
    }

//    public License findLicenseByLicense(License sub) {
//        return subRepo.findLicenseByLicense(sub).orElseThrow(() -> new LicenseNotFoundException("License by sub " + sub + " not found"));
//    }

    public void deleteLicense(Long id) {
        licenseRepo.deleteLicenseById(id);
    }

    public License findLicenseByHash(String hash) {
        return licenseRepo.findLicenseByHash(hash);
    }
}
