package fr.idan.chrono.backend.helper;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Random;

public class CryptoUtils {

    public static final String RSA = "RSA";
    public static final String LICENCE_PRIVATE_KEY = "/licence/private.key";
    public static final String LICENCE_PRIVATE_TEMP_KEY = "/temp/private.key";
    public static final String LICENCE_PRIVATE_HASH_KEY = "/licence/private_hash.key";

    public static PrivateKey getPrivateKeyFromFile(String privateKeyName) {
        try {
            // get public key from file
            URL url = CryptoUtils.class.getResource(privateKeyName);
            if (url != null) {
                File privateKeyFile = new File(url.toURI());
                byte[] privateKeyBytes = Files.readAllBytes(privateKeyFile.toPath());

                PKCS8EncodedKeySpec spec =
                        new PKCS8EncodedKeySpec(privateKeyBytes);
                KeyFactory kf = KeyFactory.getInstance(RSA);
                return kf.generatePrivate(spec);
            }
            // TODO
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encodedString) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        PrivateKey privateKey = getPrivateKeyFromFile(LICENCE_PRIVATE_KEY);
        Cipher decryptCipher = Cipher.getInstance(RSA);
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        // decode from base 64
        byte[] encryptedMessageBytes = Base64.getDecoder().decode(encodedString);

        byte[] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
        String decryptedMessage = "! Cannot decrypt message !";
        decryptedMessage = new String(decryptedMessageBytes, StandardCharsets.UTF_8);

        return decryptedMessage;
    }

    public static String encrypt(String toEncrypt) {
        PrivateKey privateKey = getPrivateKeyFromFile(LICENCE_PRIVATE_KEY);

        Cipher encryptCipher = null;
        try {
            encryptCipher = Cipher.getInstance(RSA);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            encryptCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] secretMessageBytes = toEncrypt.getBytes(StandardCharsets.UTF_8);

        try {
            byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);

            return Base64.getEncoder().encodeToString(encryptedMessageBytes);

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decryptHash(String encodedString, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        Cipher decryptCipher = Cipher.getInstance(RSA);
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        // decode from base 64
        byte[] encryptedMessageBytes = Base64.getDecoder().decode(encodedString);

        byte[] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
        String decryptedMessage = "! Cannot decrypt message !";
        decryptedMessage = new String(decryptedMessageBytes, StandardCharsets.UTF_8);

        return decryptedMessage;
    }

    public static String encryptHash(String toEncrypt, PrivateKey privateKey) {

        Cipher encryptCipher = null;
        try {
            encryptCipher = Cipher.getInstance(RSA);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            encryptCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] secretMessageBytes = toEncrypt.getBytes(StandardCharsets.UTF_8);

        try {
            byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);

            return Base64.getEncoder().encodeToString(encryptedMessageBytes);

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Code that I used to make private and public keys
    public static PrivateKey makeKey() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(RSA);
        generator.initialize(2048);
        KeyPair pair = generator.generateKeyPair();

        return pair.getPrivate();
    }

    public static String createLicenseKey(String email, String date) {
        final String s = email + "|" + date;
        final HashFunction hashFunction = Hashing.sha1();
        final HashCode hashCode = hashFunction.hashString(s, StandardCharsets.UTF_8);
        final String upper = hashCode.toString().toUpperCase();
        return group(upper);
    }

    private static String group(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            if (i % 6 == 0 && i > 0) {
                result += '-';
            }
            result += s.charAt(i);
        }
        return result;
    }

    public static String createLicence() {
        return generateRandomAlphanumeric() + "-" + generateRandomAlphanumeric() + "-" + generateRandomAlphanumeric() + "-" + generateRandomAlphanumeric();
    }

    public static String generateRandomAlphanumeric() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 4;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    public static PrivateKey getPrivateKeyFromString(String privateKeyAsString) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Read in the key into a String
        StringBuilder pkcs8Lines = new StringBuilder();
        BufferedReader rdr = new BufferedReader(new StringReader(privateKeyAsString));
        String line = "";
        while (true) {
            try {
                if (!((line = rdr.readLine()) != null))
                    break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            pkcs8Lines.append(line);
        }

        // Remove the "BEGIN" and "END" lines, as well as any whitespace
        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

        // Base64 decode the result
        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);

        // extract the private key

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = null;

        kf = KeyFactory.getInstance("RSA");

        return kf.generatePrivate(keySpec);

    }

    public PrivateKey getKeyFromString(String stored) throws GeneralSecurityException {
        PKCS8EncodedKeySpec keySpec =
                new PKCS8EncodedKeySpec(
                        Base64.getDecoder().decode(stored.getBytes(StandardCharsets.UTF_8)));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    public static String getKeyAsString(PrivateKey privateKey) {
        // get encoded form (byte array)
        byte[] privateKeyByte = privateKey.getEncoded();
        // Base64 encoded string
        return Base64.getEncoder().encodeToString(privateKeyByte);

    }
}
