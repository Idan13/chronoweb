package fr.idan.chrono.backend.repo;

import fr.idan.chrono.backend.model.License;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LicenseRepo extends JpaRepository<License, Long> {

    void deleteLicenseById(Long id);

    Optional<License> findLicenseById(Long id);

    License findLicenseByHash(String hash);
}
