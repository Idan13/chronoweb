import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home';
import {AuthGuard} from './_helpers';
import {PricingComponent} from "@app/pricing/pricing.component";
import {DownloadComponent} from "@app/download/download.component";
import {GuideComponent} from "@app/guide/guide.component";

const accountModule = () => import('./_modules/account/account.module').then(x => x.AccountModule);
const usersModule = () => import('./users/users.module').then(x => x.UsersModule);
const subscriptionModule = () => import('./_modules/subscription/subscription.module').then(x => x.SubscriptionModule);

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path:'pricing',
    component:PricingComponent
  },
  {
    path: 'users',
    loadChildren: usersModule
  },
  {
    path: 'account',
    loadChildren: accountModule
  },
  {
    path: 'subscription',
    loadChildren: subscriptionModule
  },
  {
    path: 'download',
    component: DownloadComponent,
    // TODO canActivate: [DownloadGuard]
  },
  {
    path: 'guide',
    component: GuideComponent,
  },
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'corrected' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
