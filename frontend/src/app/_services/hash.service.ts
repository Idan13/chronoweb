import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Hash} from "../_models/hash";
import {environment} from "@environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HashService {
  private backendUrl = environment.backendUrl;

  constructor(private http: HttpClient) {
  }

  public getHashes(): Observable<Hash[]> {
    return this.http.get<Hash[]>(`${this.backendUrl}/hash/all`);
  }

  public getHash(hashId: number): Observable<Hash> {
    return this.http.get<Hash>(`${this.backendUrl}/hash/find/${hashId}`);
  }

  public addHash(hash: Hash): Observable<Hash> {
    return this.http.post<Hash>(`${this.backendUrl}/hash/add`, hash);
  }

  public updateHash(hash: Hash): Observable<Hash> {
    return this.http.put<Hash>(`${this.backendUrl}/hash/update`, hash);
  }

  public deleteHash(hashId: number): Observable<void> {
    return this.http.delete<void>(`${this.backendUrl}/hash/delete/${hashId}`);
  }

}
