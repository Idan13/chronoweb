import {Injectable} from '@angular/core';
import {environment} from "@environments/environment";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {License} from "@app/_models/sub";

@Injectable({providedIn: 'root'})
export class SubscribeService {
  private backendUrl = environment.backendUrl;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    console.log("implementing subscribe service");
  }

  subscribe(email, type) {

    console.log("Subcribe : " + this.backendUrl + "/license/add");
    console.log("email =", email);
    console.log("type =", type);

    let license : License = new License(email, type);

    return this.http.post<boolean>(`${this.backendUrl}/license/add`, {license})
      .pipe(map(success => {
        console.log("subcribe was successfull ===>", success)
        return success;
      }));
  }

  unsubscribe(email, pcName, date, type) {
    console.log("Unsubcribe : " + this.backendUrl + "/license/delete");
    console.log("email =", email);
    console.log("type =", type);
    console.log("date =", date);

    return this.http.post<boolean>(`${this.backendUrl}/license/delete`, {email})
      .pipe(map(success => {
        console.log("unsubcribe was successfull ===>", success)
        return success;
      }));
  }

}
