import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Licence} from "../_models/licence";
import {environment} from "@environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LicenceService {
  private apiServerUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  public getLicences(): Observable<Licence[]> {
    return this.http.get<Licence[]>(`${this.apiServerUrl}/license/all`);
  }

  public getLicence(licenceId: number): Observable<Licence> {
    return this.http.get<Licence>(`${this.apiServerUrl}/license/find/${licenceId}`);
  }

  public addLicence(licence: Licence): Observable<Licence> {
    return this.http.post<Licence>(`${this.apiServerUrl}/license/add`, licence);
  }

  public updateLicence(licence: Licence): Observable<Licence> {
    return this.http.put<Licence>(`${this.apiServerUrl}/license/update`, licence);
  }

  public deleteLicence(licenceId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/license/delete/${licenceId}`);
  }
}
