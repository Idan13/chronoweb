﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '@environments/environment';
import {User} from '@app/_models';

@Injectable({providedIn: 'root'})
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  private backendUrl = environment.backendUrl;
  private _userTemp;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    console.log("implementing account service");
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));

    console.log("this.user= ", this.userSubject.value);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    console.log("this.userSubject= ", this.userSubject.value);
    return this.userSubject.value;
  }

  account() {
    console.log("navigate");
    this.router.navigate(['/account/login']);
  }

  login(email, password) {
    return this.http.post<User>(`${this.backendUrl}/users/authenticate`, {email, password})
      .pipe(map(user => {
        console.log("user ===>", user)
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }

  logout() {
    console.log("account.service::logout()");
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/account/login']);
  }

  register(user: User) {
    return this.http.post<User>(`${this.backendUrl}/users/register`, user)
      .pipe(map(response => {
        console.log("user ===>", response)
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(response));
        this.userSubject.next(response);
        return response;
      }));
  }

  getAll() {
    return this.http.get<User[]>(`${this.backendUrl}/users`);
  }

  getById(id: string) {
    return this.http.get<User>(`${this.backendUrl}/users/${id}`);
  }

  update(id, params) {
    return this.http.put(`${this.backendUrl}/users/${id}`, params)
      .pipe(map(x => {
        // update stored user if the logged in user updated their own record
        if (id == this.userValue.id) {
          // update local storage
          const user = {...this.userValue, ...params};
          localStorage.setItem('user', JSON.stringify(user));

          // publish updated user to subscribers
          this.userSubject.next(user);
        }
        return x;
      }));
  }

  delete(id: string) {
    return this.http.delete(`${this.backendUrl}/users/${id}`)
      .pipe(map(x => {
        // auto logout if the logged in user deleted their own record
        if (id == this.userValue.id) {
          this.logout();
        }
        return x;
      }));
  }


  get userTemp() {
    return this._userTemp;
  }

  set userTemp(value) {
    this._userTemp = value;
  }

}
