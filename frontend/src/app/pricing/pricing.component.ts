import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  p:string;
  c:string;

  constructor(private router : Router) { }

  ngOnInit(): void {
  }


  getAbonnement(type: string) {
    if(type === 'regular'){
      this.router.navigate(['/subscription/recap'], { queryParams: { type: 'regular' }});
    }
    if(type === 'pro'){
      this.router.navigate(['/subscription/recap'], { queryParams: { type: 'pro' }});
    }
  }
}
