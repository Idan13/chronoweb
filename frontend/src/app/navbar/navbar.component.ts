import {Component, OnInit} from '@angular/core';
import {User} from "@app/_models";
import {AccountService} from "@app/_services";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: User;

  constructor(private router: Router,
              private accountService: AccountService) {
    this.accountService.user.subscribe(x => this.user = x);
  }

  ngOnInit(): void {
  }

  logout() {
    console.log("logout");
    this.accountService.logout();
  }

  login() {
    console.log("display login");
    this.accountService.account();
  }

  home() {
    this.router.navigate(['/home']);
  }
}
