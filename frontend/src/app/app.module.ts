﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

// used to create fake backend
import {ErrorInterceptor, /*fakeBackendProvider,*/ JwtInterceptor} from './_helpers';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AlertComponent} from './_components';
import {HomeComponent} from './home';
import {NavbarComponent} from "@app/navbar/navbar.component";
import {CarouselComponent} from "@app/carousel/carousel.component";
import {PricingComponent} from "@app/pricing/pricing.component";
import { DownloadComponent } from './download/download.component';;
import { FooterComponent } from './footer/footer.component';;
import { GuideComponent } from './guide/guide.component'

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    NavbarComponent,
    CarouselComponent,
    PricingComponent,
    DownloadComponent,
    FooterComponent,
    GuideComponent
 ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},

    // provider used to create fake backend
    // fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
};
