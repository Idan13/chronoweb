﻿import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import {AccountService, AlertService} from '@app/_services';

@Component({
  selector: 'app-register-sub',
  templateUrl: './register.sub.component.html'
})
export class RegisterSubComponent implements OnInit {

  @Output() wantRegister = new EventEmitter<boolean>();
  @Output() email = new EventEmitter<string>();
  @Output() hasLogged = new EventEmitter<boolean>();

  form: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService,
    private alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [this.accountService.userTemp !== undefined ? this.accountService.userTemp : '', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    console.log("register this", this.form.value);
    this.accountService.register(this.form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', {keepAfterRouteChange: true});
          //Send email to parent component
          this.email.emit(this.f.email.value);
          this.hasLogged.emit(true);

        },
        error => {
          this.alertService.error(error);
          this.loading = false;
          this.hasLogged.emit(false);
        });
  }

  cancel() {
    this.accountService.userTemp = this.f.email.value;
    this.wantRegister.emit(false);
  }
}
