﻿import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService, AlertService } from '@app/_services';

@Component({
  selector: 'app-login-sub',
  templateUrl: './login.sub.component.html' })
export class LoginSubComponent implements OnInit {

    @Output() wantRegister = new EventEmitter<boolean>();
    @Output() email = new EventEmitter<string>();
    @Output() hasLogged = new EventEmitter<boolean>();

    form: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;


    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService
    ) { }

    ngOnInit() {

        console.log("init sub login");

        this.form = this.formBuilder.group({
            email: [this.accountService.userTemp !== undefined ? this.accountService.userTemp : '', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        console.log("form build");

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.accountService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    //Send email to parent component
                    this.email.emit(this.f.email.value);
                    // emit logg try result
                    this.hasLogged.emit(true);
                },
                error => {
                    console.log("error", error);
                    this.alertService.error(error);
                    this.loading = false;
                  this.hasLogged.emit(false);
                });
    }

  public register() {
    this.accountService.userTemp = this.f.email.value;
    this.wantRegister.emit(true);
  }
}
