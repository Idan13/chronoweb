import {NgModule, OnInit} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from "@app/_modules/subscription/layout.component";
import {SubscriptionRoutingModule} from "@app/_modules/subscription/subscription-routing.module";
import {RecapComponent} from './recap/recap.component';
import {PaymentComponent} from './payment/payment.component';
import {ActivatedRoute} from "@angular/router";
import {LoginSubComponent} from "@app/_modules/subscription/login/login.sub.component";
import {RegisterSubComponent} from "@app/_modules/subscription/register/register.sub.component";
import {PricingSubComponent} from './pricing/pricing.sub.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SubscriptionRoutingModule
  ],
  declarations: [
    LayoutComponent,
    RecapComponent,
    PaymentComponent,
    LoginSubComponent,
    RegisterSubComponent,
    PricingSubComponent
  ]
})
export class SubscriptionModule implements OnInit {
  // type de forfait
  type: string;


  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.type = this.route.snapshot.queryParams.type || 'regular';
  }

}


