import {Component, Input, OnInit} from '@angular/core';
import {first} from "rxjs/operators";
import {SubscribeService} from "@app/_services/subscribe.service";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.less']
})
export class PaymentComponent implements OnInit {

  @Input() private _email: string;
  @Input() private _type: string;

  constructor( private subscribeService: SubscribeService) { }

  ngOnInit(): void {
  }

  pay() {
    this.subscribeService.subscribe(this.email,this.type)
      .pipe(first())
      .subscribe(
        data => {
          // console.log("navigate to return url " , this.returnUrl);
          // this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log("error", error);
          // this.alertService.error(error);
          // this.loading = false;
        });
  }


  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }
}
