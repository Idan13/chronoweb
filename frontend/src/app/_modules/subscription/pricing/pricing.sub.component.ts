
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-pricing-sub',
  templateUrl: './pricing.sub.component.html',
  styleUrls: ['./pricing.sub.component.css']
})
export class PricingSubComponent implements OnInit {

  @Input()
  type: string;

  @Output() typeChange = new EventEmitter<string>();

  isRegular: boolean;

  isPro: boolean;

  constructor() { }

  ngOnInit(): void {
    console.log("type = ", this.type);

    console.log("this.type === 'regular' = ", this.type === 'regular');

    this.isRegular = this.type === 'regular';
    this.isPro = this.type === 'pro';
  }


  changeSub(type: string) {
    console.log("changing type to ", type);
    this.type=type;
    this.typeChange.emit(this.type.valueOf());
    this.isRegular = this.type === 'regular';
    this.isPro = this.type === 'pro';
  }

}
