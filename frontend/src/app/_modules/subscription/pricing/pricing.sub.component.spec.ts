import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pricing.SubComponent } from './pricing.sub.component';

describe('Pricing.SubComponent', () => {
  let component: Pricing.SubComponent;
  let fixture: ComponentFixture<Pricing.SubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pricing.SubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pricing.SubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
