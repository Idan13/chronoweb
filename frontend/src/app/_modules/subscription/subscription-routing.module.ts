import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LayoutComponent} from './layout.component';
import {RecapComponent} from "@app/_modules/subscription/recap/recap.component";

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      {path: 'recap', component: RecapComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscriptionRoutingModule {
}
