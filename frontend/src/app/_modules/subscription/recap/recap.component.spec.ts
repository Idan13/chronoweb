import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RecapComponent} from "@app/_modules/subscription/recap/recap.component";


describe('RecapComponent', () => {
  let component: RecapComponent;
  let fixture: ComponentFixture<RecapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
