import {Component, OnInit, ViewChild} from '@angular/core';
import {AccountService} from "@app/_services";
import {ActivatedRoute} from "@angular/router";
import {PaymentComponent} from "@app/_modules/subscription/payment/payment.component";

@Component({
  selector: 'app-recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.less']
})
export class RecapComponent implements OnInit {

  isLogged: boolean = false;
  wantRegister: boolean = false;
  type: string;
  email: string

  @ViewChild('paymentComponent')
  paymentComponent: PaymentComponent;

  constructor(private accountService: AccountService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // get subcribe type from route parameters or default to 'regular'
    this.type = this.route.snapshot.queryParams.type || 'regular';

    const user = this.accountService.userValue;
    if (user) {
      // authorised so return true
      this.isLogged = true;
      this.paymentComponent.email = this.email;
      this.paymentComponent.type = this.type;
    }

  }

  wantedRegister($event: boolean) {
    this.wantRegister = $event.valueOf();
  }

  setEmail($event: string) {
    this.email = $event.valueOf();

    console.log("set email",this.email);
  }

  setType($event: string) {
    this.type = $event.valueOf();

    console.log("set type",this.type);
  }

  refreshUserLogged($event: boolean) {
    const user = this.accountService.userValue;
    if (user){
      // authorised so return true
      this.isLogged = true;
      this.paymentComponent.email = this.email;
      this.paymentComponent.type = this.type;
    }
    else
      this.isLogged = false;
  }
}
