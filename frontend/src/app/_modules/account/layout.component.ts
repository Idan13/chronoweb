﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AccountService } from '@app/_services';

@Component({ templateUrl: 'layout.component.html' })
export class LayoutComponent {
    constructor(
        private router: Router,
        private accountService: AccountService
    ) {
        console.log("layoutComponent");
        // redirect to home if already logged in
        if (this.accountService.userValue) {
            console.log("redirect to home because already logged in");
            this.router.navigate(['/']);
        }
    }
}
