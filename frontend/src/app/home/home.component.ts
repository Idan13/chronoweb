﻿import { Component } from '@angular/core';

import { User } from '@app/_models';
import { AccountService } from '@app/_services';
import {Router} from "@angular/router";

@Component({ templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css'] })
export class HomeComponent {
    //user: User;

    constructor(private router : Router) {
        //this.user = this.accountService.userValue;
    }

  goToPrincing() {
    this.router.navigate(['/pricing']);
  }
}
