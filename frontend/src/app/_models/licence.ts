export interface Licence {
  id:number;
  date:Date;
  pcName:string;
  clientId:string;
}
